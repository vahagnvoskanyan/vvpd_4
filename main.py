import argparse

def itallic(string: str) -> str:
    if string.count("*") >= 2:
        string = string.replace('*', '<em>', 1)
        string = string.replace('*', '</em>')
    return string


def bold(string: str) -> str:
    if string.count("*") >= 4:
        string = string.replace('**', '<strong>', 1)
        string = string.replace('**', '</strong>')
    return string


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--itallic')
    parser.add_argument('-b', '--bold')
    args = parser.parse_args()
    if args.itallic:
        print(itallic(args.itallic))
    if args.bold:
        print(bold(args.bold))

if __name__ == "__main__":
    main()
